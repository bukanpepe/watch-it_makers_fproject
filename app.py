from flask import Flask, jsonify, request, redirect, url_for, render_template, send_file
from flask_cors import CORS
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from datetime import datetime
import decimal
import string
import random
import hashlib

app = Flask(__name__)
CORS(app)

conn_str = 'postgresql://postgres:Putranurelva2804?@localhost:5000/cinema_DB'
engine = create_engine(conn_str, echo=False)

########## UTILITY FUNCTIONS ##########
def generate_key():
    letters_and_digits = string.ascii_letters + string.digits
    result_str = ''.join((random.choice(letters_and_digits) for i in range(20)))
    return result_str

def generate_session(_id, _key):
    try:
        with engine.connect() as connection:
            qry = text(f"INSERT INTO public.logged_user (user_id, userkey) VALUES ('{_id}', '{_key}')")
            run = connection.execute(qry)
            print("Session Generated")
    except Exception as e:
        return jsonify(str(e))

def user_validation(userkey):
    res = []
    with engine.connect() as connection:
        qry = text(f"SELECT user_id, first_name, balance FROM public.logged_user JOIN public.user USING(user_id) WHERE userkey = '{userkey}'")
        run = connection.execute(qry)
        for i in run:
            res.append({'user_id' : i[0], 'f_name' : i[1], 'balance' : int(i[2])})
        return res[0]

@app.route('/images/<file>')
def get_banner(file):
    f = file
    mimetype = 'image/png'
    return send_file(f, mimetype=mimetype)

@app.route('/scripts/<file>')
def get_file(file):
    f = file
    mimetype = "application/javascript"
    return send_file(f, mimetype=mimetype)

                ########## RETRIEVE HTML ##########

########## GENERAL INTERFACE ##########
@app.route('/')
def defaultUrl():
    return redirect(url_for('indexNow_playing'))

@app.route('/index')
def index():
    return redirect(url_for('indexNow_playing'))

@app.route('/index/nowplaying', methods=['GET'])
def indexNow_playing():
    return render_template('index.html')

@app.route('/index/<title>', methods=['GET', 'POST'])
def indexDetails(title):
    return render_template('index.html', title=title)

@app.route('/index/search', methods=['GET', 'POST'])
def indexSearch():
    title = request.args.get('q')
    return render_template('index.html', title=title)
    
@app.route('/index/comingsoon')
def indexUpcoming():
    return render_template('index.html')

@app.route('/index/mostwatched')
def indexMost_watched():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def user_login():
    if request.method == 'POST':
        body = request.form
        _email = body['e']
        _pwd = body['p']
        _pwdencoded = hashlib.md5(_pwd.encode())

        with engine.connect() as connection:
            validate = text(f"SELECT email_address, user_password FROM public.user WHERE email_address = '{_email}'")
            runValid = connection.execute(validate)
            for i in runValid:
                user_email = i[0]
                user_pass = i[1]
            try:
                if user_email == _email and user_pass == _pwdencoded.hexdigest():
                    validateUser = text(f"SELECT user_id FROM public.user WHERE email_address = '{_email}'")
                    runValidUser = connection.execute(validateUser)
                    for j in runValidUser:
                        _user_id = j[0]

                    validateSession = text(f"SELECT * FROM public.logged_user WHERE user_id='{_user_id}'")
                    runValidSession = connection.execute(validateSession)
                    for k in runValidSession:
                        user_session = k[0]
                        user_key = k[1]
                    
                    try:
                        if user_session == _user_id:
                            return redirect(url_for('userDash', userkey=user_key))
                    except Exception:
                        _key = generate_key()
                        session = generate_session(_user_id, _key)
                        
                        return redirect(url_for('userDash', userkey=_key))
                else:
                    return render_template('login.html', alert="Email and Password does not match")
            except Exception as e:
                print(str(e))
                return render_template('login.html', alert=f"Account with E-mail address : {_email} does not exist,\nplease create an account")
                
    return render_template('login.html')

@app.route('/logout/<userkey>')
def user_logout(userkey):
    try:
        with engine.connect() as connection:
            qry = text(f"DELETE FROM public.logged_user WHERE userkey = '{userkey}'")
            run = connection.execute(qry)
        return redirect(url_for('indexNow_playing'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/new/user', methods=['GET', 'POST'])
def create_user():
    if request.method == 'POST':
        body = request.form
        _email = body['e']
        _pwd = str(body['p'])
        _repwd = body['rep']
        _first_name = body['first_name']
        _last_name = body['last_name']
        _address = body['address']
        if _pwd != _repwd:
            return render_template('create_acc.html', alert='Re-enter Password does not match the Password')
        else:
            with engine.connect() as connection:
                validate = text(f"SELECT email_address, user_password FROM public.user WHERE email_address = '{_email}'")
                runValid = connection.execute(validate)

                for i in runValid:
                    user_email = i[0]
                    user_pass = i[1]
                try:
                    if user_email == None or user_email == '':
                        return 'error'
                    else :
                        return render_template('create_acc.html', alert='Email address already being used for another account')
                except Exception:
                    _pwdencoded = hashlib.md5(_pwd.encode())

                    createAcc = text(f"INSERT INTO public.user (first_name, last_name, email_address, user_address, user_password)\
                                    VALUES ('{_first_name}', '{_last_name}', '{_email}', '{_address}', '{_pwdencoded.hexdigest()}')")
                    runCreate = connection.execute(createAcc)

                    validateNew = text(f"SELECT user_id FROM public.user WHERE email_address = '{_email}'")
                    runValidNew = connection.execute(validateNew)
                    for j in runValidNew:
                        _user_id = j[0]

                    _key = generate_key()
                    session = generate_session(_user_id, _key)

                    return redirect(url_for('userDash', userkey=_key))

    return render_template('create_acc.html')

########## USER INTERFACE ##########
@app.route('/dashboard/<userkey>', methods=['GET'])
def userDash(userkey):

    valid = user_validation(userkey)
    
    try:
        if valid['user_id'] == None or valid['user_id'] == '':
            return 'error'
        else:
            return render_template('user_dash.html', user=valid['f_name'], userkey=userkey, balance=valid['balance'], user_id=valid['user_id'])

    except Exception:
        return render_template('create_acc.html', alert="Please create an account first")


@app.route('/dashboard/comingsoon/<userkey>')
def userComingSoon(userkey):
    valid = user_validation(userkey)
    try:
        if valid['user_id'] == None or valid['user_id'] == '':
            return 'error'
        else:
            return render_template('user_dash.html', user=valid['f_name'], userkey=userkey, balance=valid['balance'], user_id=valid['user_id'])

    except Exception:
        return render_template('create_acc.html', alert="Please create an account first")

@app.route('/dashboard/mostwatched/<userkey>')
def userMostWatched(userkey):
    valid = user_validation(userkey)
    try:
        if valid['user_id'] == None or valid['user_id'] == '':
            return 'error'
        else:
            return render_template('user_dash.html', user=valid['f_name'], userkey=userkey, balance=valid['balance'], user_id=valid['user_id'])

    except Exception:
        return render_template('create_acc.html', alert="Please create an account first")

@app.route('/dashboard/schedule/<title>/<userkey>', methods=['GET'])
def movieSchedule(title, userkey):
    valid = user_validation(userkey)
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT play_date\
                            FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE title = '{title}' and play_date = CURRENT_DATE")
            run = connection.execute(qry)

            for i in run:
                play_date = i[0].strftime("%d %B ,%Y")
            try:
                if play_date == None or play_date == '':
                    return 'error'
                else:
                    if valid['user_id'] == None or valid['user_id'] == '':
                        return 'error'
                    else:
                        return render_template('user_dash.html', title=title, user=valid['f_name'], userkey=userkey, balance=valid['balance'], user_id=valid['user_id'])
            except Exception:
                return render_template('user_dash.html', message=f"{title} currently does not have any show schedule", user=valid['f_name'], userkey=userkey, balance=valid['balance'], user_id=valid['user_id'])
    except Exception:
        return render_template('create_acc.html', alert="Please create an account first")


@app.route('/purchase/<schedule_id>/<userkey>', methods=['GET', 'POST'])
def purchase(schedule_id, userkey) :
    valid = user_validation(userkey)
    user_balance = valid['balance']
    user_id = valid['user_id']

    if request.method == 'POST':
        body = request.form
        qty = int(body['qty'])
        with engine.connect() as connection:
            movDet = text(f"SELECT price FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE schedule_id = {schedule_id}")
            runMovDet = connection.execute(movDet)
            for i in runMovDet:
                price = int(i[0])
            totPrice = qty * price
            if user_balance < totPrice:
                return render_template('user_purchase.html', schedule_id=schedule_id, userkey=userkey, alert="Balance is not enough to purchase ticket,\nPlease top up your balance", user_id=user_id, balance=user_balance, user=valid['f_name'])
            else:
                updateTrx = text(f"INSERT INTO public.transaction (user_id, purchase) VALUES({user_id}, {totPrice})")
                runUpdateTrx = connection.execute(updateTrx)

                curBal = user_balance - totPrice
                updateBal = text(f"UPDATE public.user SET balance = {curBal} WHERE user_id = {user_id}")
                runUpdateBal = connection.execute(updateBal)

                return redirect(url_for('userDash', userkey=userkey))
    else:
        if user_id != None or user_id != '' :
            return render_template('user_purchase.html', schedule_id=schedule_id, userkey=userkey, user_id=user_id, balance=user_balance,  user=valid['f_name'])
        else:           
            return render_template('create_acc.html', alert="Please create an account first")

@app.route('/dashboard/search/<userkey>', methods=['GET'])
def userSearchFunc(userkey):
    search = request.args.get('q')
    valid = user_validation(userkey)
    try:
        if valid['user_id'] == None or valid['user_id'] == '':
            return 'error'
        else:
            return render_template('user_dash.html', title=search, user=valid['f_name'], userkey=userkey, balance=valid['balance'], user_id=valid['user_id'])

    except Exception:
        return render_template('create_acc.html', alert="Please create an account first")

@app.route('/profile/<user_id>', methods=['GET'])
def userProfile(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT user_id, userkey, balance FROM public.logged_user JOIN public.user USING(user_id) WHERE user_id = {user_id}")
            run = connection.execute(qry)
            for i in run:
                userkey = i[1]
                balance = int(i[2])
            return render_template('user_profile.html', user_id=user_id, userkey=userkey, balance=balance)
    except Exception as e:
        return jsonify(str(e))

@app.route('/profile/edit/<user_id>', methods=['POST'])
def editProfile(user_id):
    body = request.form
    f_name = body['f_name']
    if not f_name:
        f_name = 'NULL'
    l_name = body['l_name']
    if not l_name:
        l_name = 'NULL'
    address = body['address']
    if not address:
        address = 'NULL'
    email_address = body['email_address']
    if not email_address:
        email_address = 'NULL'
    password = body['password']
    if not password:
        password = 'NULL'
    else :
        password = hashlib.md5(password.encode())
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT first_name, last_name, user_address, email_address, user_password FROM public.user WHERE user_id={user_id}")
            run = connection.execute(qry)

            if f_name != 'NULL' :
                updateF = text(f"UPDATE public.user SET first_name = '{f_name}' WHERE user_id = {user_id}")
                runUpdateF = connection.execute(updateF)
            if l_name != 'NULL' :
                updateL = text(f"UPDATE public.user SET last_name = '{l_name}' WHERE user_id = {user_id}")
                runUpdateL = connection.execute(updateL)
            if address != 'NULL':
                updateA = text(f"UPDATE public.user SET user_address = '{address}' WHERE user_id = {user_id}")
                runUpdateA = connection.execute(updateA)
            if email_address != 'NULL':
                updateE = text(f"UPDATE public.user SET email_address = '{email_address}' WHERE user_id = {user_id}")
                runUpdateE = connection.execute(updateA)
            if password != 'NULL':
                updateP = text(f"UPDATE public.user SET user_password = '{password.hexdigest()}' WHERE user_id = {user_id}")
                runUpdateP = connection.execute(updateA)

        return redirect(url_for('userProfile', user_id=user_id))
            
    except Exception as e:
        return jsonify(str(e))

@app.route('/profile/topup/<user_id>', methods=['POST'])
def userTopUp(user_id):
    try:
        body = request.form
        value = int(body['value'])
        with engine.connect() as connection:
            getBal = text(f"SELECT balance FROM public.user WHERE user_id = {user_id}")
            runGetBal = connection.execute(getBal)

            for i in runGetBal:
                curBal = int(i[0])

            sumBal = curBal + value
            addBal = text(f"UPDATE public.user SET balance = {sumBal} WHERE user_id = {user_id}")
            runAddBal = connection.execute(addBal)

            updateTrx = text(f"INSERT INTO public.transaction (user_id, top_up) VALUES({user_id}, {value})")
            runUpdateTrx = connection.execute(updateTrx)

        return redirect(url_for('userProfile', user_id=user_id))
            
    except Exception as e:
        return jsonify(str(e))

        ###### RETRIEVE JSON ##########

@app.route('/movie/nowplaying', methods=['GET'])
def movie():
    display = []
    try:
        with engine.connect() as connection:
            get_movie = text("SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, img_src FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE status = 'Now Playing'\
                            GROUP BY 1,2,3,5 ORDER BY 1")
            movie_res = connection.execute(get_movie)
            for i in movie_res:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "genre" : i[3], "poster" : i[4]})
            return jsonify(display)

    except Exception as e:
        return jsonify(error=str(e))

@app.route('/movie/<title>', methods=['GET', 'POST'])
def movieDetails(title):
    display = []
    try:
        with engine.connect() as connection:
            get_detail = text(f"SELECT movie_id, title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', ') as genre, img_src FROM movie\
                                    JOIN movie_cat USING(movie_id)\
                                    JOIN category USING (category_id)\
                                    WHERE title = '{title}'\
                                    GROUP BY 1,2,3,4,5")
            detail_res = connection.execute(get_detail)
            for i in detail_res:
                display.append({"title" : i[1], "desc" : i[2], "directed" : i[3], "release" : i[4], "duration" : i[5], "genre" : i[6], "poster" : i[7]})
        return jsonify(display)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/movie/search/<value>', methods=['GET'])
def searchApp(value):
    display = []
    try:
        with engine.connect() as connection:
            if value.isdigit():
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', '), img_src\
                            FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                            WHERE (title LIKE '%{value}%' OR description LIKE '%{value}%' OR directed_by LIKE '%{value}%' OR release_year = '{value}')\
                            GROUP BY 1,2,3,4,5,7")
            else:
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', '), img_src\
                            FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                            WHERE (title LIKE '%{value}%' OR description LIKE '%{value}%' OR directed_by LIKE '%{value}%')\
                            GROUP BY 1,2,3,4,5,7")

            detail_res = connection.execute(get_detail)
            for i in detail_res:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "release_year" : i[3], "duration_mins" : i[4], "genre" : i[5], "poster" : i[6]})
            return jsonify(display)

    except Exception as e:
        return jsonify(str(e))

@app.route('/movie/comingsoon')
def movieUpcoming():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, img_src\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE status = 'Coming Soon'\
                            GROUP BY 1,2,3,5 ORDER BY 1")
            movies = connection.execute(qry)
            for i in movies:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "genre" : i[3], "poster" : i[4]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/movie/mostwatched')
def movieMost_watched():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT m1.title, m1.directed_by, m1.img_src, \
                            (SELECT string_agg(cat_name, ', ') as genre\
                            FROM movie as m2\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING(category_id)\
                            WHERE m1.title = m2.title), SUM(ticket_qty) as total\
                        FROM movie as m1\
                        JOIN schedule as Sched USING(movie_id)\
                        JOIN ticket USING(schedule_id)\
                        GROUP BY 1,2,3\
                        ORDER BY 5 DESC\
                        LIMIT 5")
            movies = connection.execute(qry)
            for i in movies:
                display.append({"title" : i[0], "directed_by" : i[1], "poster" : i[2], "genre" : i[3], "totalTix" : i[4]})
            return jsonify(display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/schedule/<title>/<userkey>', methods=['GET'])
def getSchedule(title, userkey):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT play_date, studio_id, start_time, price, schedule_id\
                            FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE title = '{title}' and play_date = CURRENT_DATE and start_time >= CURRENT_TIME")
            run = connection.execute(qry)
            start_time = []
            schedule_id = []
            for i in run:
                play_date = i[0].strftime("%d %B, %Y")
                studio_id = i[1]
                start_time.append(i[2].strftime("%H:%M"))
                price = int(i[3])
                schedule_id.append(i[4])

            movie = text(f"SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, img_src, duration_mins\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE title = '{title}'\
                            GROUP BY 1,2,3,5,6 ORDER BY 1")
            getMovie = connection.execute(movie)
            for j in getMovie:
                title = j[0]
                description = j[1]
                directed_by = j[2]
                genre = j[3]
                img_src = j[4]
                duration_mins = j[5]
            return jsonify(play_date=play_date, studio_id=studio_id, start_time=start_time, price=price, title=title, description=description, directed_by=directed_by, genre=genre, poster=img_src, duration_mins=duration_mins, schedule_id=schedule_id, userkey=userkey)
            
    except Exception as e:
        return jsonify(str(e))

@app.route('/schedule/purchase/<schedule_id>', methods=['GET'])
def purchaseTix(schedule_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT play_date, studio_id, start_time, price, movie_id\
                            FROM schedule\
                            JOIN movie USING(movie_id)\
                            JOIN day_schedule USING(day_id)\
                            WHERE schedule_id ={schedule_id}")
            run = connection.execute(qry)
            for i in run:
                play_date = i[0].strftime("%d %B, %Y")
                studio_id = i[1]
                start_time = i[2].strftime("%H:%M")
                price = int(i[3])
                movie_id = i[4]

            movie = text(f"SELECT title, description, directed_by, string_agg(cat_name, ', ') as genre, img_src, duration_mins\
                            FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE movie_id = {movie_id}\
                            GROUP BY 1,2,3,5,6 ORDER BY 1")
            getMovie = connection.execute(movie)
            for j in getMovie:
                title = j[0]
                description = j[1]
                directed_by = j[2]
                genre = j[3]
                img_src = j[4]
                duration_mins = j[5]
            return jsonify(play_date=play_date, studio_id=studio_id, start_time=start_time, price=price, title=title, description=description, directed_by=directed_by, genre=genre, poster=img_src, duration_mins=duration_mins, schedule_id=schedule_id)
            
    except Exception as e:
        return jsonify(str(e))
 

@app.route('/user/<user_id>', methods=['GET'])
def getProfile(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT user_id, userkey, first_name, last_name, user_address, email_address, balance FROM public.logged_user JOIN public.user USING(user_id) WHERE user_id = {user_id}")
            run = connection.execute(qry)
            for i in run:
                userkey = i[1]
                f_name = i[2]
                l_name = i[3]
                user_address = i[4]
                email_address = i[5]
                balance = int(i[6])
            return jsonify(user_id=user_id, userkey=userkey, f_name=f_name, l_name=l_name, user_address=user_address, email_address=email_address, balance=balance)
    except Exception as e:
        return jsonify(str(e))

if __name__ == '__main__':
    app.run()