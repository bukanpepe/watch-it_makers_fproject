const defaultUrl = "http://127.0.0.1:9003/index";
const currentUrl = window.location.href;
const hiddenTitle = document.getElementById('hidden').innerHTML;

if(currentUrl == defaultUrl + '/nowplaying') {
    getMovie();
}
    
function getMovie() {
        
    let i = 0;
    fetch('http://127.0.0.1:9003/movie/nowplaying')
    .then(response => response.json())
    .then(result => {
        while(i < result.length) {
            let cont = document.getElementById('content');
            let h2 = document.createElement('h2');
            let p1 = document.createElement('p');
            let p2 = document.createElement('p');
            let p3 = document.createElement('p');
            let p4 = document.createElement('p');
            let img = document.createElement('img');
            let a =document.createElement('a');
                
            let movTitle = result[i].title;
            img.src = result[i].poster;
            img.alt = movTitle;
            img.title = movTitle;
            cont.appendChild(img);

            a.href='/index/' + movTitle;
            a.appendChild(h2);
            h2.innerHTML = movTitle;
            cont.appendChild(a);

            let movDirect = result[i].directed_by;
            p2.innerHTML = 'Director : '.bold() + movDirect;
            cont.appendChild(p2);

            let movGenre = result[i].genre;
            p3.innerHTML = 'Genre : '.bold() + movGenre;
            cont.appendChild(p3);

            i++;
        }
    })
};

    if(currentUrl == defaultUrl + '/' + hiddenTitle.replace(/ /gi, "%20")) {
        searchMoviebyTitle(hiddenTitle);
    }

    function searchMoviebyTitle(title) {
    let url = 'http://127.0.0.1:9003/movie/' + title;
    fetch(url)
    .then(response => response.json())
    .then(result => {
            let cont = document.getElementById('content');
            let h2 = document.createElement('h2');
            let p1 = document.createElement('p');
            let p2 = document.createElement('p');
            let p3 = document.createElement('p');
            let p4 = document.createElement('p');
            let p5 = document.createElement('p');
            let img = document.createElement('img');
            let a =document.createElement('a');
                
            let movTitle = result[0].title;
            img.src = result[0].poster;
            img.alt = movTitle;
            img.title = movTitle;
            cont.appendChild(img);

            h2.innerHTML = movTitle;                
            cont.appendChild(h2);
            
            let movDesc = result[0].desc;
            p1.innerHTML = movDesc;
            cont.appendChild(p1);

            let movDirect = result[0].directed;
            p2.innerHTML = 'Director : '.bold() + movDirect;
            cont.appendChild(p2);

            let movGenre = result[0].genre;
            p3.innerHTML = 'Genre : '.bold() + movGenre;
            cont.appendChild(p3);

            let movYear = result[0].release;
            p4.innerHTML = 'Release Year : '.bold() + movYear;
            cont.appendChild(p4);

            let movDur = result[0].duration;
            p5.innerHTML = 'Duration : '.bold() + movDur + ' mins';
            cont.appendChild(p5);
        })
};

if(currentUrl == defaultUrl + '/comingsoon') {
        upcomingMovie();
    }

    function upcomingMovie() {
    let url = 'http://127.0.0.1:9003/movie/comingsoon';
    let i = 0;
    fetch(url)
    .then(response => response.json())
    .then(result => {
        while(i < result.length) {
            let cont = document.getElementById('content');
            let h2 = document.createElement('h2');
            let p1 = document.createElement('p');
            let p2 = document.createElement('p');
            let p3 = document.createElement('p');
            let p4 = document.createElement('p');
            let p5 = document.createElement('p');
            let img = document.createElement('img');
            let a =document.createElement('a');
                
            let movTitle = result[i].title;
            img.src = result[i].poster;
            img.alt = movTitle;
            img.title = movTitle;
            cont.appendChild(img);

            a.href='/index/' + movTitle;
            a.appendChild(h2);
            h2.innerHTML = movTitle;                
            cont.appendChild(a);
            
            let movDirect = result[i].directed_by;
            p2.innerHTML = 'Director : '.bold() + movDirect;
            cont.appendChild(p2);

            let movGenre = result[i].genre;
            p3.innerHTML = 'Genre : '.bold() + movGenre;
            cont.appendChild(p3);

            i++;
        }
    });
}

if(currentUrl == defaultUrl + '/mostwatched') {
        mostwatchedMovie();
    }

    function mostwatchedMovie() {
    let url = 'http://127.0.0.1:9003/movie/mostwatched';
    let i = 0;
    fetch(url)
    .then(response => response.json())
    .then(result => {
        while(i < result.length) {
            let cont = document.getElementById('content');
            let h2 = document.createElement('h2');
            let p1 = document.createElement('p');
            let p2 = document.createElement('p');
            let p3 = document.createElement('p');
            let p4 = document.createElement('p');
            let p5 = document.createElement('p');
            let img = document.createElement('img');
            let a =document.createElement('a');
                
            let movTitle = result[i].title;
            img.src = result[i].poster;
            img.alt = movTitle;
            img.title = movTitle;
            cont.appendChild(img);

            a.href='/index/' + movTitle;
            a.appendChild(h2);
            h2.innerHTML = movTitle;                
            cont.appendChild(a);
            
            let movDirect = result[i].directed_by;
            p2.innerHTML = 'Director : '.bold() + movDirect;
            cont.appendChild(p2);

            let movGenre = result[i].genre;
            p3.innerHTML = 'Genre : '.bold() + movGenre;
            cont.appendChild(p3);

            let sumTix = result[i].totalTix;
            p4.innerHTML = 'Watched by : ' + sumTix + ' moviegoers';
            cont.appendChild(p4);
            
            i++;
        }
    });
}
