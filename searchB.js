if(currentUrl == defaultUrl + '/search?q=' + hiddenTitle.replace(/ /gi, "%20")) {
    lookUpMovie(hiddenTitle);    
    }

function lookUpMovie(value) {
let url = 'http://127.0.0.1:9003/movie/search/' + value;
let i = 0;
fetch(url)
.then(response => response.json())
.then(result => {
    while(i < result.length) {
        let cont = document.getElementById('content');
        let h2 = document.createElement('h2');
        let p1 = document.createElement('p');
        let p2 = document.createElement('p');
        let p3 = document.createElement('p');
        let p4 = document.createElement('p');
        let p5 = document.createElement('p');
        let img = document.createElement('img');
        let a = document.createElement('a');
            
        let movTitle = result[i].title;
        img.src = result[i].poster;
        img.alt = movTitle;
        img.title = movTitle;
        cont.appendChild(img);

        a.href='/index/' + movTitle;
        a.appendChild(h2);
        h2.innerHTML = movTitle;
        cont.appendChild(a);
        
        let movDesc = result[i].description;
        p1.innerHTML = movDesc;
        cont.appendChild(p1);

        let movDirect = result[i].directed_by;
        p2.innerHTML = 'Director : '.bold() + movDirect;
        cont.appendChild(p2);

        let movGenre = result[i].genre;
        p3.innerHTML = 'Genre : '.bold() + movGenre;
        cont.appendChild(p3);

        let movYear = result[i].release_year;
        p4.innerHTML = 'Release Year : '.bold() + movYear;
        cont.appendChild(p4);

        let movDur = result[i].duration_mins;
        p5.innerHTML = 'Duration : '.bold() + movDur + ' mins';
        cont.appendChild(p5);

        i++;
        }
    });
}
